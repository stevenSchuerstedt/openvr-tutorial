#version 330 core

in vec3 position;

out vec3 texcoord;

uniform mat4 matrix;
void main(){

texcoord = position;

vec4 WVP_Pos = matrix * vec4(position, 1.0);
//gl_Position = WVP_Pos.xyww;
gl_Position = matrix * vec4(position, 1.0);


}