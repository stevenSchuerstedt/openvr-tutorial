openvr tutorial
---

This is a simple openvr test scene to show the core principles of the api. It uses openvr in combination with opengl. It is a little bit messy, as I just put it together real quickly. If there is interest I will clean it up more nicely and explain things more in detail. I think there is a need for a more elaborate tutorial, as the official wiki is really short and leaves alot of things to be figured out by yourself. There is a sample program avaible at the official page, but it has around 2000 lines of code and is sparsly commented. Also I dont know any other openvr tutorial so far. 

What is openvr?
---

openvr is a sdk from valve, that enables developers to communicate with the steamvr runtime and build virtual reality applications. So in order to use openvr you need to have steam, steamvr and of course a virtual reality headset, like the htc vive. More information on openvr: https://github.com/ValveSoftware/openvr 

how does it work?
---

In the following I try to explain the core principles of openvr used with opengl. Other graphic apis, like vulcan or directx could work slightly differently. openvr consists of different interfaces, that all have a unique purpose. The VRCompositor interface can display graphics to the hmd. In short, you create two framebuffers with a texture for each eye and render the whole scene in this texture. The scene is rendered with special openvr matrices, to change the view and projection matrix accordingly to the position of the hmd. Then these textures can be submitted to the corresponding eye. This is done with:

*vr::VRCompositor()->Submit(vr::Eye, &EyeTexture);*

In traditional computer graphics the position of a vertex is calculated with the following matrix product:

**position_out = projection * view * model * position_in;**

The projection and view matrix is now provided from openvr, to change the view of the scene according to the head position. 

**position_out = ProjectionEyeLeftorRight * eyePosLeftorRight * HMDPosition * model * position_in;**

The projection matrix is splitted into two matrices now, you can get the ProjectionEyeLeftorRight with:

*GetProjectionMatrix( EVREye eEye, float fNearZ, float fFarZ )*

Be careful as it returns the matrix in steams own matrix format. You need to convert it, for example in a glm matrix. (see the code example)

The other matrix is retreived with: 

*GetEyeToHeadTransform(nEye);*

This matrices can retreived at the initialization of the program, as they normally are not changed afterwards.

The view matrix now is the position of the hmd. As the position is changing all the time when you are looking around, it needs constantly to be refreshed, so it should be included in the render loop. 

 *vr::VRCompositor()->WaitGetPoses(TrackedDevicePose, vr::unMaxTrackedDeviceCount, NULL, 0);*
 
 This method returns an array of tracked position in TrackedDevicePose. You need to verify the pose with *bPoseIsValid* and then the HMDPosition is the inverse of the retreived matrix. 
 
 That's it for the basics. For a detailed implementation please look at the provided code example. If you have some questions or are unsure how a certain aspect of the api is working, please leave a comment or send me a message. 
 
 What is in the code example?
 ---
 
 In the provided code example is a vr scene with a cube and a skybox. The controllers are rendered as well, when avaible. If u press the bottom button the controller will change its appearance into a cube. In addition to the hmd, the scene is rendered to a screen filling quad, which just uses the framebuffer textures. I included a simple texture loader and shader manager to get everything running. 
 
 
 For more information about my other gitlab projects visit my website: [steven.schuerstedt.com](https://steven.schuerstedt.com/?page_id=24)
