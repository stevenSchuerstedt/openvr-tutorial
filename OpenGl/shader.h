#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "include/GL/glew.h"

class Shader
{
public:
	unsigned int ID;
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	void use();


};