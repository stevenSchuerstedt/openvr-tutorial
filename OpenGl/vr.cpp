#include "vr.h"

using namespace glm;

GLuint CGLRenderModel::m_rendermodelMatrixLocation;
GLuint CGLRenderModel::shader_rendermodel;

vr::VRActionHandle_t m_actionDrawControllerAsCube = vr::k_ulInvalidActionHandle;

vr::VRActionSetHandle_t m_actionsetDemo = vr::k_ulInvalidActionSetHandle;


void ThreadSleep(unsigned long nMilliseconds)
{
#if defined(_WIN32)
  ::Sleep(nMilliseconds);
#elif defined(POSIX)
  usleep(nMilliseconds * 1000);
#endif
}

CGLRenderModel::CGLRenderModel(const std::string & sRenderModelName)
  : m_sModelName(sRenderModelName)
{
  m_glIndexBuffer = 0;
  m_glVertArray = 0;
  m_glVertBuffer = 0;
  m_glTexture = 0;
}

bool CGLRenderModel::BInit(const vr::RenderModel_t & vrModel, const vr::RenderModel_TextureMap_t & vrDiffuseTexture) {
  // create and bind a VAO to hold state for this model
  glGenVertexArrays(1, &m_glVertArray);
  glBindVertexArray(m_glVertArray);

  // Populate a vertex buffer
  glGenBuffers(1, &m_glVertBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, m_glVertBuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vr::RenderModel_Vertex_t) * vrModel.unVertexCount, vrModel.rVertexData, GL_STATIC_DRAW);

  // Identify the components in the vertex buffer
  GLint posAttrib = glGetAttribLocation(shader_rendermodel, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(vr::RenderModel_Vertex_t), (void *)offsetof(vr::RenderModel_Vertex_t, vPosition));
  //normals are not needed so far
  //glEnableVertexAttribArray(1);
  //glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vr::RenderModel_Vertex_t), (void *)offsetof(vr::RenderModel_Vertex_t, vNormal));
  GLint texAttrib = glGetAttribLocation(shader_rendermodel, "texcoord");
  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(vr::RenderModel_Vertex_t), (void *)offsetof(vr::RenderModel_Vertex_t, rfTextureCoord));

  // Create and populate the index buffer
  glGenBuffers(1, &m_glIndexBuffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_glIndexBuffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * vrModel.unTriangleCount * 3, vrModel.rIndexData, GL_STATIC_DRAW);

  glBindVertexArray(0);

  // create and populate the texture

  GLint texpos = glGetAttribLocation(shader_rendermodel, "tex");
  glUniform1i(texpos, 0);

  glGenTextures(1, &m_glTexture);
  glBindTexture(GL_TEXTURE_2D, m_glTexture);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, vrDiffuseTexture.unWidth, vrDiffuseTexture.unHeight,
    0, GL_RGBA, GL_UNSIGNED_BYTE, vrDiffuseTexture.rubTextureMapData);


  glGenerateMipmap(GL_TEXTURE_2D);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  GLfloat fLargest;
  glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);

  glBindTexture(GL_TEXTURE_2D, 0);

  m_unVertexCount = vrModel.unTriangleCount * 3;


  //init cube
  GLfloat vertices[] = {
    //	X		Y		Z	  R		G	  B		U	  V
    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
    0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f
  };
  glGenVertexArrays(1, &m_glCubeArray);
  glBindVertexArray(m_glCubeArray);


  //vertex Attributes
  GLuint vbo2;
  glGenBuffers(1, &vbo2);
  glBindBuffer(GL_ARRAY_BUFFER, vbo2);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

  //load layout location of position
  GLint posAttrib2 = glGetAttribLocation(shader_rendermodel, "position");
  glVertexAttribPointer(posAttrib2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), 0);
  glEnableVertexAttribArray(posAttrib2);

  //color
  GLint colAttrib2 = glGetAttribLocation(shader_rendermodel, "color");
  glVertexAttribPointer(colAttrib2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 3));
  glEnableVertexAttribArray(colAttrib2);

  //texture
  GLint tex2 = glGetAttribLocation(shader_rendermodel, "texcoord");
  glVertexAttribPointer(tex2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 6));
  glEnableVertexAttribArray(tex2);

  return true;
}

void CGLRenderModel::DrawCube(GLuint texture) {

  glBindVertexArray(m_glCubeArray);

  glBindTexture(GL_TEXTURE_2D, texture);

  glDrawArrays(GL_TRIANGLES, 0, 36);

  glBindVertexArray(0);

}

void CGLRenderModel::DrawController() {

  glBindVertexArray(m_glVertArray);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, m_glTexture);

  glDrawElements(GL_TRIANGLES, m_unVertexCount, GL_UNSIGNED_SHORT, 0);

  glBindVertexArray(0);

}

bool openvr::init() { 
	glfwInit();
  width = 1400; height = 900;
	window = glfwCreateWindow(width, height, "Opengl", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	GLenum nGlewError = glewInit();

	if (nGlewError != GLEW_OK)
	{
		printf("%s - Error initializing GLEW! %s\n", __FUNCTION__, glewGetErrorString(nGlewError));
		return false;
	}
	glGetError(); // to clear the error caused deep in GLEW

	textureLoader::Init();

	vr::EVRInitError eError = vr::VRInitError_None;
	m_pHMD = vr::VR_Init(&eError, vr::VRApplication_Scene);
	if (eError != vr::VRInitError_None)
	{
		m_pHMD = NULL;
		char buf[1024];
		sprintf_s(buf, sizeof(buf), "Unable to init VR runtime: %s", vr::VR_GetVRInitErrorAsEnglishDescription(eError));
		return false;
	}

	m_fNearClip = 0.1f;
	m_fFarClip = 30.0f;

  m_model = mat4(1.0f);
	//initGL
	createShader();
	setupScene();
	setupCameras();
	setupStereoRenderTargets();

	//init Compositor
	vr::EVRInitError peError = vr::VRInitError_None;

	if (!vr::VRCompositor())
	{
		printf("Compositor initialization failed. See log file for details\n");
		return false;
	}
  vr::EVRInputError error;

  //init inputs
  error = vr::VRInput()->SetActionManifestPath("D:/Schuerstedt/GIT/opengl/OpenGl/hellovr_actions.json");

  vr::VRInput()->GetActionHandle("/actions/demo/in/DrawControllerAsCube", &m_actionDrawControllerAsCube);

  vr::VRInput()->GetActionSetHandle("/actions/demo", &m_actionsetDemo);

  vr::VRInput()->GetActionHandle("/actions/demo/out/Haptic_Left", &m_rHand[Left].m_actionHaptic);
  vr::VRInput()->GetInputSourceHandle("/user/hand/left", &m_rHand[Left].m_source);
  vr::VRInput()->GetActionHandle("/actions/demo/in/Hand_Left", &m_rHand[Left].m_actionPose);

  vr::VRInput()->GetActionHandle("/actions/demo/out/Haptic_Right", &m_rHand[Right].m_actionHaptic);
  vr::VRInput()->GetInputSourceHandle("/user/hand/right", &m_rHand[Right].m_source);
  vr::VRInput()->GetActionHandle("/actions/demo/in/Hand_Right", &m_rHand[Right].m_actionPose);

  drawControllerAsCube = false;

	return true;
}

void openvr::createShader() {
	Shader lshader_scene("vr_scene.vert", "vr_scene.frag");
  Shader lshader_quad("fbovertex.vert", "fbofragment.frag");
  Shader lshader_skybox("skybox.vert", "skybox.frag");
  Shader lshader_rendermodel("rendermodel.vert", "rendermodel.frag");
	//lshader_scene.use();
	shader_scene = lshader_scene.ID;
  shader_quad = lshader_quad.ID;
  shader_skybox = lshader_skybox.ID;
  CGLRenderModel::shader_rendermodel = lshader_rendermodel.ID;

  m_nSceneMatrixLocation = glGetUniformLocation(shader_scene, "matrix");
  m_assimpMatrixLocation = glGetUniformLocation(shader_assimp, "matrix");
  m_nSkyboxMatrixLocation = glGetUniformLocation(shader_skybox, "matrix");
  CGLRenderModel::m_rendermodelMatrixLocation = glGetUniformLocation(CGLRenderModel::shader_rendermodel, "matrix");
  lshader_scene.use();
}

void openvr::setupSkybox() {
  GLfloat skyboxVertices[] = {
    // positions          
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f,  1.0f
  };

  //skybox texture
  std::vector<std::string> faces
  {
    "skybox/right.jpg",
    "skybox/left.jpg",
    "skybox/top.jpg",
    "skybox/bottom.jpg",
    "skybox/front.jpg",
    "skybox/back.jpg"
  };
  texture_skybox = textureLoader::loadCubemap(faces);


  glGenVertexArrays(1, &vao_skybox);
  glBindVertexArray(vao_skybox);

  //vertex Attributes
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

  //load layout location of position
  GLint posAttrib = glGetAttribLocation(shader_skybox, "position");
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(posAttrib);

  GLint texpos = glGetAttribLocation(shader_skybox, "skybox");
  glUniform1i(texpos, 0);

}

void openvr::setupScene() {

	GLfloat vertices[] = {
//	X		Y		Z	  R		G	  B		U	  V
	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f
	};

	glGenVertexArrays(1, &vao_scene);
	glBindVertexArray(vao_scene);

	//Texture
	texture_jessica = textureLoader::loadImage("img_test.png");
  

	//vertex Attributes
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	//load layout location of position
	GLint posAttrib = glGetAttribLocation(shader_scene, "position");
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), 0);
	glEnableVertexAttribArray(posAttrib);

	//color
	GLint colAttrib = glGetAttribLocation(shader_scene, "color");
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 3));
	glEnableVertexAttribArray(colAttrib);

	//texcoords
	GLint texAttrib = glGetAttribLocation(shader_scene, "texcoord");
	glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 6));
	glEnableVertexAttribArray(texAttrib);

	//sampler uniform
	GLint texpos = glGetAttribLocation(shader_scene, "tex");
	glUniform1i(texpos, 0);

	glBindVertexArray(0);


  //companion window

  glUseProgram(shader_quad);
  GLfloat screenQuadVertices[] =
  {//	x		y	r	g	b
    -1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0,
    1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 0.0,
    1.0,  1.0, 0.0, 1.0, 1.0, 1.0, 1.0,
    -1.0,  1.0, 1.0, 1.0, 1.0, 0.0, 1.0
  };


  glGenVertexArrays(1, &vao_quad);
  glBindVertexArray(vao_quad);


  //vertex Attributes
  GLuint vbo2;
  glGenBuffers(1, &vbo2);
  glBindBuffer(GL_ARRAY_BUFFER, vbo2);
  glBufferData(GL_ARRAY_BUFFER, sizeof(screenQuadVertices), screenQuadVertices, GL_DYNAMIC_DRAW);

  //load layout location of position
  GLint posAttrib2 = glGetAttribLocation(shader_quad, "position");
  glVertexAttribPointer(posAttrib2, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GL_FLOAT), 0);
  glEnableVertexAttribArray(posAttrib2);

  //color
  GLint colAttrib2 = glGetAttribLocation(shader_quad, "color");
  glVertexAttribPointer(colAttrib2, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 2));
  glEnableVertexAttribArray(colAttrib2);

  //texcoords
  GLint texAttrib2 = glGetAttribLocation(shader_quad, "texcoord");
  glVertexAttribPointer(texAttrib2, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GL_FLOAT), (void*)(sizeof(GL_FLOAT) * 5));
  glEnableVertexAttribArray(texAttrib2);

  //sampler uniform
  GLint texpos2 = glGetAttribLocation(shader_quad, "tex");
  glUniform1i(texpos2, 0);

  glBindVertexArray(0);

  setupSkybox();
}
void openvr::setupCameras() {

	m_mat4ProjectionLeft = GetHMDMatrixProjectionEye(vr::Eye_Left);
	m_mat4ProjectionRight = GetHMDMatrixProjectionEye(vr::Eye_Right);

	m_mat4eyePosLeft = GetHMDMatrixPoseEye(vr::Eye_Left);
	m_mat4eyePosRight = GetHMDMatrixPoseEye(vr::Eye_Right);

  m_mat4HMDPose = mat4(1.0f);

}

mat4 openvr::GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye) {

	vr::HmdMatrix44_t mat = m_pHMD->GetProjectionMatrix(nEye, m_fNearClip, m_fFarClip);

	//return a glm mat4
	return mat4(
		mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
		mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
	);

}

mat4 openvr::GetHMDMatrixPoseEye(vr::Hmd_Eye nEye)
{

	vr::HmdMatrix34_t matEyeRight = m_pHMD->GetEyeToHeadTransform(nEye);
	mat4 matrixObj(
		matEyeRight.m[0][0], matEyeRight.m[1][0], matEyeRight.m[2][0], 0.0,
		matEyeRight.m[0][1], matEyeRight.m[1][1], matEyeRight.m[2][1], 0.0,
		matEyeRight.m[0][2], matEyeRight.m[1][2], matEyeRight.m[2][2], 0.0,
		matEyeRight.m[0][3], matEyeRight.m[1][3], matEyeRight.m[2][3], 1.0f
	);
	
	return inverse(matrixObj);
}

void openvr::setupStereoRenderTargets() {

	m_pHMD->GetRecommendedRenderTargetSize(&m_nRenderWidth, &m_nRenderHeight);

	createFrameBuffer(m_nRenderWidth, m_nRenderHeight, leftEyeDesc);
	createFrameBuffer(m_nRenderWidth, m_nRenderHeight, rightEyeDesc);

}

void openvr::createFrameBuffer(int nWidth, int nHeight, FramebufferDesc &framebufferDesc) {

	glGenFramebuffers(1, &framebufferDesc.m_nRenderFramebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferDesc.m_nRenderFramebufferId);

	
	glGenRenderbuffers(1, &framebufferDesc.m_nDepthBufferId);
	glBindRenderbuffer(GL_RENDERBUFFER, framebufferDesc.m_nDepthBufferId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, nWidth, nHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, framebufferDesc.m_nDepthBufferId);

	glGenTextures(1, &framebufferDesc.m_nRenderTextureId);
	glBindTexture(GL_TEXTURE_2D, framebufferDesc.m_nRenderTextureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, nWidth, nHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebufferDesc.m_nRenderTextureId, 0);

	glBindTexture(GL_TEXTURE_2D, 0);


	// check FBO status
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Framebuffer creation successful." << std::endl;
	}
	else {
		std::cout << "Framebuffer creation FAILED." << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


}

mat4 openvr::getCurrentViewProjectionMatrix(vr::Hmd_Eye nEye)
{
  mat4 matMVP;
  if (nEye == vr::Eye_Left)
  {
    matMVP = m_mat4ProjectionLeft * m_mat4eyePosLeft * m_mat4HMDPose;
  }
  else if (nEye == vr::Eye_Right)
  {
    matMVP = m_mat4ProjectionRight * m_mat4eyePosRight *  m_mat4HMDPose;
  }

  return matMVP;
}

mat4 openvr::getSkyboxMatrix(vr::Hmd_Eye nEye)
{
  mat4 matMVP;
  if (nEye == vr::Eye_Left)
  {
    //remove translation from pose
    mat4 pose = mat4(mat3(m_mat4HMDPose));
    matMVP = m_mat4ProjectionLeft * m_mat4eyePosLeft * pose;
  }
  else if (nEye == vr::Eye_Right)
  {
    mat4 pose = mat4(mat3(m_mat4HMDPose));
    matMVP = m_mat4ProjectionRight * m_mat4eyePosRight * pose;
  }

  return matMVP;
}

void openvr::renderScene(vr::Hmd_Eye nEye) {

  glClearColor(0.3f, 0.2f, 0.5f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //skybox
  glDepthMask(GL_FALSE);
  glUseProgram(shader_skybox);
  glUniformMatrix4fv(m_nSkyboxMatrixLocation, 1, GL_FALSE, value_ptr(getSkyboxMatrix(nEye)));
  glBindVertexArray(vao_skybox);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture_skybox);
  glDrawArrays(GL_TRIANGLES, 0, 36);
  glDepthMask(GL_TRUE);

  //cube

  glEnable(GL_DEPTH_TEST);
  glUseProgram(shader_scene);

  m_model = rotate(m_model, radians(0.1f), vec3(1.0, 0.0, 0.0));

  glUniformMatrix4fv(m_nSceneMatrixLocation, 1, GL_FALSE, value_ptr(getCurrentViewProjectionMatrix(nEye) * m_model));
  glBindVertexArray(vao_scene);
  glBindTexture(GL_TEXTURE_2D, texture_jessica);
  glDrawArrays(GL_TRIANGLES, 0, 36);
  glBindVertexArray(0);
  glDisable(GL_DEPTH_TEST);
  

  //render Controllers if avaible

  glEnable(GL_DEPTH_TEST);
  glUseProgram(CGLRenderModel::shader_rendermodel);

  for (EHand eHand = Left; eHand <= Right; ((int&)eHand)++)
  {
    if (!m_rHand[eHand].m_bShowController || !m_rHand[eHand].m_pRenderModel)
      continue;

    mat4 matDeviceToTracking = m_rHand[eHand].m_rmat4Pose;
    glUniformMatrix4fv(CGLRenderModel::m_rendermodelMatrixLocation, 1, GL_FALSE, value_ptr(getCurrentViewProjectionMatrix(nEye) * matDeviceToTracking));

    if (drawControllerAsCube) {
      matDeviceToTracking = scale(matDeviceToTracking, vec3(0.25, 0.25, 0.25));
      glUniformMatrix4fv(CGLRenderModel::m_rendermodelMatrixLocation, 1, GL_FALSE, value_ptr(getCurrentViewProjectionMatrix(nEye) * matDeviceToTracking));
      m_rHand[eHand].m_pRenderModel->DrawCube(texture_jessica);
    }
    else {
      m_rHand[eHand].m_pRenderModel->DrawController();
    }

   
  }

  glDisable(GL_DEPTH_TEST);

  
}

void openvr::renderStereoTargets() {

  glBindFramebuffer(GL_FRAMEBUFFER, leftEyeDesc.m_nRenderFramebufferId);
  glViewport(0, 0, m_nRenderWidth, m_nRenderHeight);
  renderScene(vr::Eye_Left);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glBindFramebuffer(GL_FRAMEBUFFER, rightEyeDesc.m_nRenderFramebufferId);
  glViewport(0, 0, m_nRenderWidth, m_nRenderHeight);
  renderScene(vr::Eye_Right);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

mat4 openvr::convertSteamVRMatrixToMat4(const vr::HmdMatrix34_t &matPose) {
  mat4 matrixObj(
    matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
    matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
    matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
    matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
  );
  return matrixObj;
}

void openvr::updateHMDMatrixPose() {

  vr::VRCompositor()->WaitGetPoses(m_rTrackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);
  
  m_rmat4DevicePose[vr::k_unTrackedDeviceIndex_Hmd] = convertSteamVRMatrixToMat4(m_rTrackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking);
     
  if (m_rTrackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
  {
    m_mat4HMDPose = m_rmat4DevicePose[vr::k_unTrackedDeviceIndex_Hmd];
    m_mat4HMDPose = inverse(m_mat4HMDPose);
  }

}

void openvr::drawScreenQuad() {

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glViewport(0, 0, width/2, height);

    glBindTexture(GL_TEXTURE_2D, rightEyeDesc.m_nRenderTextureId);
    glUseProgram(shader_quad);
    glBindVertexArray(vao_quad);
    glDrawArrays(GL_POLYGON, 0, 4);

    glViewport(width/2, 0, width, height);

    glBindTexture(GL_TEXTURE_2D, leftEyeDesc.m_nRenderTextureId);
    glUseProgram(shader_quad);
    glBindVertexArray(vao_quad);
    glDrawArrays(GL_POLYGON, 0, 4);

    glBindVertexArray(0);

}


CGLRenderModel *openvr::FindOrLoadRenderModel(const char *pchRenderModelName) {

  CGLRenderModel *pRenderModel = NULL;
  for (std::vector< CGLRenderModel * >::iterator i = m_vecRenderModels.begin(); i != m_vecRenderModels.end(); i++)
  {
    if (!_stricmp((*i)->GetName().c_str(), pchRenderModelName))
    {
      pRenderModel = *i;
      break;
    }
  }

  // load the model if we didn't find one
  if (!pRenderModel)
  {
    vr::RenderModel_t *pModel;
    vr::EVRRenderModelError error;

	//wait until the loading is completely finished 
    while (1)
    {
      error = vr::VRRenderModels()->LoadRenderModel_Async(pchRenderModelName, &pModel);
      if (error != vr::VRRenderModelError_Loading)
        break;

      ThreadSleep(1);
    }

    if (error != vr::VRRenderModelError_None)
    {
      std::cout << ("Unable to load render model %s - %s\n", pchRenderModelName, vr::VRRenderModels()->GetRenderModelErrorNameFromEnum(error)) << std::endl;
      return NULL; // move on to the next tracked device
    }

    vr::RenderModel_TextureMap_t *pTexture;
    
    while (1)
    {
      error = vr::VRRenderModels()->LoadTexture_Async(pModel->diffuseTextureId, &pTexture);
      if (error != vr::VRRenderModelError_Loading)
        break;

      ThreadSleep(1);
    }
       
    if (error != vr::VRRenderModelError_None)
    {
      std::cout << ("Unable to load render texture id:%d for render model %s\n", pModel->diffuseTextureId, pchRenderModelName) << std::endl;
      vr::VRRenderModels()->FreeRenderModel(pModel);
      return NULL; // move on to the next tracked device
    }

    pRenderModel = new CGLRenderModel(pchRenderModelName);
    if (!pRenderModel->BInit(*pModel, *pTexture))
    {
      std::cout << ("Unable to create GL model from render model %s\n", pchRenderModelName) << std::endl;
      delete pRenderModel;
      pRenderModel = NULL;
    }
    else
    {
      m_vecRenderModels.push_back(pRenderModel);
    }
    vr::VRRenderModels()->FreeRenderModel(pModel);
    vr::VRRenderModels()->FreeTexture(pTexture);
  }
  return pRenderModel;

}

std::string openvr::GetTrackedDeviceString(vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError *peError)
{
  uint32_t unRequiredBufferLen = vr::VRSystem()->GetStringTrackedDeviceProperty(unDevice, prop, NULL, 0, peError);
  if (unRequiredBufferLen == 0)
    return "";

  char *pchBuffer = new char[unRequiredBufferLen];
  unRequiredBufferLen = vr::VRSystem()->GetStringTrackedDeviceProperty(unDevice, prop, pchBuffer, unRequiredBufferLen, peError);
  std::string sResult = pchBuffer;
  delete[] pchBuffer;
  return sResult;
}

void openvr::handleInput() {


  vr::VRActiveActionSet_t actionSet = { 0 };
  actionSet.ulActionSet = m_actionsetDemo;
  vr::VRInput()->UpdateActionState(&actionSet, sizeof(actionSet), 1);

  //drawControllerAsCube when button is pressed
  vr::InputDigitalActionData_t actionData;
  vr::VRInput()->GetDigitalActionData(m_actionDrawControllerAsCube, &actionData, sizeof(actionData), vr::k_ulInvalidInputValueHandle);

  drawControllerAsCube = actionData.bActive && actionData.bState;

  m_rHand[Left].m_bShowController = true;
  m_rHand[Right].m_bShowController = true;

  for (EHand eHand = Left; eHand <= Right; ((int&)eHand)++)
  {
    vr::InputPoseActionData_t poseData;
    vr::EVRInputError error;
    error = vr::VRInput()->GetPoseActionData(m_rHand[eHand].m_actionPose, vr::TrackingUniverseStanding, 0, &poseData, sizeof(poseData), vr::k_ulInvalidInputValueHandle);
   

    if (vr::VRInput()->GetPoseActionData(m_rHand[eHand].m_actionPose, vr::TrackingUniverseStanding, 0, &poseData, sizeof(poseData), vr::k_ulInvalidInputValueHandle) != vr::VRInputError_None
      || !poseData.bActive || !poseData.pose.bPoseIsValid)
    {
      m_rHand[eHand].m_bShowController = false;
    }
    else
    {
      m_rHand[eHand].m_rmat4Pose = convertSteamVRMatrixToMat4(poseData.pose.mDeviceToAbsoluteTracking);

      vr::InputOriginInfo_t originInfo;
      if (vr::VRInput()->GetOriginTrackedDeviceInfo(poseData.activeOrigin, &originInfo, sizeof(originInfo)) == vr::VRInputError_None
        && originInfo.trackedDeviceIndex != vr::k_unTrackedDeviceIndexInvalid)
      {
        std::string sRenderModelName = GetTrackedDeviceString(originInfo.trackedDeviceIndex, vr::Prop_RenderModelName_String);
        if (sRenderModelName != m_rHand[eHand].m_sRenderModelName)
        {
          m_rHand[eHand].m_pRenderModel = FindOrLoadRenderModel(sRenderModelName.c_str());
          m_rHand[eHand].m_sRenderModelName = sRenderModelName;
        }
      }
    }
  }

}

void openvr::runMainLoop(){

	while (!glfwWindowShouldClose(window)) {
		
    handleInput();

    updateHMDMatrixPose();

	renderStereoTargets();

    drawScreenQuad();

		vr::Texture_t leftEyeTexture = { (void*)(uintptr_t)leftEyeDesc.m_nRenderTextureId, vr::TextureType_OpenGL, vr::ColorSpace_Gamma };
		vr::VRCompositor()->Submit(vr::Eye_Left, &leftEyeTexture);
		vr::Texture_t rightEyeTexture = { (void*)(uintptr_t)rightEyeDesc.m_nRenderTextureId, vr::TextureType_OpenGL, vr::ColorSpace_Gamma };
		vr::VRCompositor()->Submit(vr::Eye_Right, &rightEyeTexture);

		glfwSwapBuffers(window);
		glfwPollEvents();

    
	}


}


int main() {

	openvr *op = new openvr();

	op->init();
	
	op->runMainLoop();
}
