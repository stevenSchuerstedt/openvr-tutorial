#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 color;

out vec2 Texcoord;

uniform mat4 matrix;

void main(){

Texcoord = texcoord; 
gl_Position = matrix * vec4(position.xyz, 1.0);

}