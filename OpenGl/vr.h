#pragma once
//OpenGL Function Loader
#include "include/GL/glew.h"
//OpenGL Context + Window
#include "include/GLFW/glfw3.h"
#include "shader.h"
//OpenGL Mathematics
#include "include/glm/glm.hpp"
#include "include/glm/gtc/matrix_transform.hpp"
#include "include/glm/gtc/type_ptr.hpp"
#include "include/glm/gtc/matrix_inverse.hpp"

//OpenGL Texture Loader
#include "textureLoader.h"

//VR
#include "include/openvr.h"


class CGLRenderModel
{
public:
  CGLRenderModel(const std::string & sRenderModelName);
  bool BInit(const vr::RenderModel_t & vrModel, const vr::RenderModel_TextureMap_t & vrDiffuseTexture);
  void DrawController();
  void DrawCube(GLuint texture);
  const std::string & GetName() const { return m_sModelName; }

  static GLuint shader_rendermodel;
  static GLuint m_rendermodelMatrixLocation;

private:
  GLuint m_glVertBuffer;
  GLuint m_glIndexBuffer;
  GLuint m_glVertArray;
  GLuint m_glCubeArray;
  GLuint m_glTexture;
  GLsizei m_unVertexCount;
  std::string m_sModelName;
};

class openvr {

	GLFWwindow *window;
  int width;
  int height;
	vr::IVRSystem *m_pHMD;
	struct FramebufferDesc
	{
		GLuint m_nDepthBufferId;
		GLuint m_nRenderTextureId;
		GLuint m_nRenderFramebufferId;
	};
	FramebufferDesc leftEyeDesc;
	FramebufferDesc rightEyeDesc;

	uint32_t m_nRenderWidth;
	uint32_t m_nRenderHeight;
	

  glm::mat4 m_model;

	glm::mat4 m_mat4HMDPose;
	glm::mat4 m_mat4eyePosLeft;
	glm::mat4 m_mat4eyePosRight;

	glm::mat4 m_mat4ProjectionCenter;
	glm::mat4 m_mat4ProjectionLeft;
	glm::mat4 m_mat4ProjectionRight;

	float m_fNearClip;
	float m_fFarClip;

	GLuint shader_scene;
  GLuint shader_quad;
  GLuint shader_skybox;
  GLuint shader_assimp;
  

	GLuint vao_scene;
  GLuint vao_quad;
  GLuint vao_skybox;
  GLuint texture_jessica;
  GLuint texture_skybox;

  GLuint m_nSceneMatrixLocation;
  GLuint m_nSceneModelLocation;
  GLuint m_nSkyboxMatrixLocation;
  GLuint m_assimpMatrixLocation;
  

  vr::TrackedDevicePose_t m_rTrackedDevicePose[vr::k_unMaxTrackedDeviceCount];
  glm::mat4 m_rmat4DevicePose[vr::k_unMaxTrackedDeviceCount];

  bool drawControllerAsCube;

public:
	bool init();
	void runMainLoop();
	void createShader();
	void setupScene();
  void setupSkybox();
  void setupCameras();
	void setupStereoRenderTargets();
	void renderStereoTargets();

  void renderScene(vr::Hmd_Eye nEye);

  void drawScreenQuad();

	void updateHMDMatrixPose();
	glm::mat4 GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye);
	glm::mat4 GetHMDMatrixPoseEye(vr::Hmd_Eye nEye);

  glm::mat4 getCurrentViewProjectionMatrix(vr::Hmd_Eye nEye);
  glm::mat4 getSkyboxMatrix(vr::Hmd_Eye nEye);

  glm::mat4 convertSteamVRMatrixToMat4(const vr::HmdMatrix34_t &matPose);

	void createFrameBuffer(int nWidth, int nHeight, FramebufferDesc &framebufferDesc);

  //controller

  CGLRenderModel *FindOrLoadRenderModel(const char *pchRenderModelName);
  void handleInput();
  std::string GetTrackedDeviceString(vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError *peError = NULL);

  struct ControllerInfo_t
  {
    vr::VRInputValueHandle_t m_source = vr::k_ulInvalidInputValueHandle;
    vr::VRActionHandle_t m_actionPose = vr::k_ulInvalidActionHandle;
    vr::VRActionHandle_t m_actionHaptic = vr::k_ulInvalidActionHandle;
    glm::mat4 m_rmat4Pose;
    CGLRenderModel *m_pRenderModel = nullptr;
    std::string m_sRenderModelName;
    bool m_bShowController;
  };

  enum EHand
  {
    Left = 0,
    Right = 1,
  };
  ControllerInfo_t m_rHand[2];

  std::vector< CGLRenderModel * > m_vecRenderModels;
};