#pragma once
#include"include/GL/glew.h"
#include "include/IL/ilut.h"
#include "include/IL/il.h"
#include <vector>

class textureLoader {
public:
	static GLuint loadImage(const char* theFileName);
  static GLuint loadCubemap(std::vector<std::string> faces);
	static void Init();
};